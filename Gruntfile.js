module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
	        options: {
	            sourceMap: true
	        },
	        dist: {
	            files: {
	                'css/style.css': 'sass/custom.scss'
	            }
	        }
	    },
	    autoprefixer: {
            dist: {
                files: {
                    'css/style.css': 'css/style.css'
                }
            }  
        },
		watch: {
			css: {
				files: '**/*.scss', 
				tasks: ['sass', 'autoprefixer']
			}
		}
	});
	//grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-sass'); 
	grunt.loadNpmTasks('grunt-contrib-watch');
  	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.registerTask('default',['watch']);
}